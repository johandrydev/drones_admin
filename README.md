# Drone Delivery API

This project is a REST API that allows clients to communicate with a fleet of drones. The drones are capable of delivering small loads, such as medications.

## Introduction

The drones have the following properties:

* Serial number (100 characters max)
* Model (Lightweight, Middleweight, Cruiserweight, Heavyweight)
* Weight limit (500gr max)
* Battery capacity (percentage)
* State (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING)

The medications that the drones can deliver have the following properties:

* Name (allowed only letters, numbers, '-', '_')
* Weight
* Code (allowed only upper case letters, underscore and numbers)
* Image (picture of the medication case)

## Requirements

* Node.js v18+
* npm v9+
* redis

## Setup Locally

1. Clone the repository.

```bash
 git clone https://gitlab.com/johandrydev/drones_admin
```

2. Install the dependencies: In this case you cloud use your favorite package manager, in this case I'm using pnpm.

- Redis: You could follow the official [Redis Quick Start](https://redis.io/docs/getting-started/)

- Install pnpm
  ```bash
  npm install -g pnpm
  ```
- Install the dependencies
  ```bash
  pnpm install
  ```
3. Run the server
  ```bash
  pnpm start
  ```
In case of development porpuses you could use the following command
  ```bash
  pnpm run dev
  ```

## Run the tests

To run the tests you could use the following command
  ```bash
  pnpm run test
  ```

## Setup to run with Docker and Docker Compose
If you have docker I have created a docker-compose file to run the project with redis and the server. With this you don't need to install anything else in your machine. Just need to run the following command:
```bash
docker compose up -d
```

# API

The REST API provides the following endpoints:

## Drones

* ### Get drones

This endpoint returns a list of all the drones.

```curl
  GET	http://localhost:3001/api/drones
```

#### **Request**
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| state | filter of drones by state | string | optional |

#### Example Request
```curl
  GET	http://localhost:3001/api/drones?state=IDLE
```

#### JSON Body

| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| serialNumber | It is the identification of drone | string | (Required) |
| model | It is the model of drone (Lightweight, Middleweight, Cruiserweight, Heavyweight) | string | (Required) |
| weightLimit | the weight limit of drone | number | (Required) |
| batteryCapacity | the battery capacity of drone | number | (Required) |
| state | the state of drone (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING) | string | (Required) |
| items | the items that the drone is carrying | array | (Required) |


#### Example JSON Response
```json
[
	{
		"serialNumber": "976460fb-816c-47ba-b654-d1528c4fed38",
		"model": "Lightweight",
		"weightLimit": 450,
		"batteryCapacity": 100,
		"state": "IDLE",
		"items": []
	},
	{
		"serialNumber": "664d2145-0c7f-4e54-8e55-c930cadf270f",
		"model": "Lightweight",
		"weightLimit": 400,
		"batteryCapacity": 100,
		"state": "LOADING",
		"items": [
			{
				"uuid": "b389c69c-8aa8-437c-9729-5e7706ce7cca",
				"name": "CLOTRIMAZOL",
				"weight": 100,
				"code": "TEST_CLO_123",
				"image": "test"
			},
			{
				"uuid": "cd37d839-a2ac-4f30-b676-577ea2f4f3de",
				"name": "Omeprazol",
				"weight": 150,
				"code": "TEST_OME_123",
				"image": "test_ome"
			},
			{
				"uuid": "a93c2c92-3706-429c-8fc1-bcb796586056",
				"name": "Loratadina",
				"weight": 50,
				"code": "TEST_LOR_123",
				"image": "test_lor"
			}
		]
	}
]
```

* ### Get a drone
This endpoint returns a drone by serial number.

```curl
  GET	http://localhost:3001/api/drones/:serialNumber
```
#### Example Request
```curl
  GET	http://localhost:3001/api/drones/976460fb-816c-47ba-b654-d1528c4fed38
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| serialNumber | It is the identification of drone | string | (Required) |
| model | It is the model of drone (Lightweight, Middleweight, Cruiserweight, Heavyweight) | string | (Required) |
| weightLimit | the weight limit of drone | number | (Required) |
| batteryCapacity | the battery capacity of drone | number | (Required) |
| state | the state of drone (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING) | string | (Required) |
| items | the items that the drone is carrying | array | (Required) |

#### Example JSON Response
```json
{
  "serialNumber": "976460fb-816c-47ba-b654-d1528c4fed38",
  "model": "Lightweight",
  "weightLimit": 450,
  "batteryCapacity": 100,
  "state": "IDLE",
  "items": []
}
```
* ### Create a drone

This endpoint creates a new drone.

```curl
  POST	http://localhost:3001/api/drones
```
#### Body request
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| model | It is the model of drone (Lightweight, Middleweight, Cruiserweight, Heavyweight) | string | (Required) |
| weightLimit | the weight limit of drone | number | (Required) |
| batteryCapacity | the battery capacity of drone | number | (Required) |
| state | the state of drone (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING) | string | (Required) |

#### Example JSON Request
```json
{
  "model": "Lightweight",
  "weightLimit": 450,
  "batteryCapacity": 100,
  "state": "IDLE"
}
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| serialNumber | It is the identification of drone | string | (Required) |
| model | It is the model of drone (Lightweight, Middleweight, Cruiserweight, Heavyweight) | string | (Required) |
| weightLimit | the weight limit of drone | number | (Required) |
| batteryCapacity | the battery capacity of drone | number | (Required) |
| state | the state of drone (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING) | string | (Required) |
| items | the items that the drone is carrying | array | (Required) |

#### Example JSON Response
```json
{
  "serialNumber": "976460fb-816c-47ba-b654-d1528c4fed38",
  "model": "Lightweight",
  "weightLimit": 450,
  "batteryCapacity": 100,
  "state": "IDLE",
  "items": []
}
```

* ### Update a drone

This endpoint updates all drone information.

```curl
  PUT	http://localhost:3001/api/drones/:serialNumber
```
#### Example Request
```curl
  PUT	http://localhost:3001/api/drones/976460fb-816c-47ba-b654-d1528c4fed38
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| model | It is the model of drone (Lightweight, Middleweight, Cruiserweight, Heavyweight) | string | (Required) |
| weightLimit | the weight limit of drone | number | (Required) |
| batteryCapacity | the battery capacity of drone | number | (Required) |
| state | the state of drone (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING) | string | (Required) |
| items | the items that the drone is carrying | array | (Required) |

#### Example JSON Request
```json
{
  "model": "Lightweight",
  "weightLimit": 450,
  "batteryCapacity": 100,
  "state": "IDLE",
  "items": []
}
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| serialNumber | It is the identification of drone | string | (Required) |
| model | It is the model of drone (Lightweight, Middleweight, Cruiserweight, Heavyweight) | string | (Required) |
| weightLimit | the weight limit of drone | number | (Required) |
| batteryCapacity | the battery capacity of drone | number | (Required) |
| state | the state of drone (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING) | string | (Required) |
| items | the items that the drone is carrying | array | (Required) |

#### Example JSON Response
```json
{
  "serialNumber": "976460fb-816c-47ba-b654-d1528c4fed38",
  "model": "Lightweight",
  "weightLimit": 450,
  "batteryCapacity": 100,
  "state": "IDLE",
  "items": []
}
```

* ### Delete a drone

This endpoint deletes a drone by serial number.

```curl
  DELETE	http://localhost:3001/api/drones/:serialNumber
```

### Example Request
```curl
  DELETE	http://localhost:3001/api/drones/976460fb-816c-47ba-b654-d1528c4fed38
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| message | It is the message of the response | string | (Required) |

#### Example JSON Response
```json
{
  "message": "Drone deleted successfully"
}
```

* ### Load medications to a drone

This endpoint loads medications to a drone.

```curl
  PATCH	http://localhost:3001/api/drones/:serialNumber
```

#### Example Request
```curl
  PATCH	http://localhost:3001/api/drones/976460fb-816c-47ba-b654-d1528c4fed38
```

#### Body request
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| uuid | It is the identification of medication | string | (Required) |
| name | It is the name of medication | string | (Required) |
| weight | the weight of medication | number | (Required) |
| code | the code of medication | string | (Required) |
| image | the image of medication | string | (Required) |

#### Example JSON Request
```json
[
  {
    "uuid": "b389c69c-8aa8-437c-9729-5e7706ce7cca",
    "name": "CLOTRIMAZOL",
    "weight": 100,
    "code": "TEST_CLO_123",
    "image": "test"
  },
  {
    "uuid": "cd37d839-a2ac-4f30-b676-577ea2f4f3de",
    "name": "Omeprazol",
    "weight": 150,
    "code": "TEST_OME_123",
    "image": "test_ome"
  },
  {
    "uuid": "a93c2c92-3706-429c-8fc1-bcb796586056",
    "name": "Loratadina",
    "weight": 50,
    "code": "TEST_LOR_123",
    "image": "test_lor"
  }
]
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| serialNumber | It is the identification of drone | string | (Required) |
| model | It is the model of drone (Lightweight, Middleweight, Cruiserweight, Heavyweight) | string | (Required) |
| weightLimit | the weight limit of drone | number | (Required) |
| batteryCapacity | the battery capacity of drone | number | (Required) |
| state | the state of drone (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING) | string | (Required) |
| items | the items that the drone is carrying | array | (Required) |

#### Example JSON response

```json
{
  "serialNumber": "976460fb-816c-47ba-b654-d1528c4fed38",
  "model": "Lightweight",
  "weightLimit": 450,
  "batteryCapacity": 100,
  "state": "IDLE",
  "items": [
    {
      "uuid": "b389c69c-8aa8-437c-9729-5e7706ce7cca",
      "name": "CLOTRIMAZOL",
      "weight": 100,
      "code": "TEST_CLO_123",
      "image": "test"
    },
    {
      "uuid": "cd37d839-a2ac-4f30-b676-577ea2f4f3de",
      "name": "Omeprazol",
      "weight": 150,
      "code": "TEST_OME_123",
      "image": "test_ome"
    },
    {
      "uuid": "a93c2c92-3706-429c-8fc1-bcb796586056",
      "name": "Loratadina",
      "weight": 50,
      "code": "TEST_LOR_123",
      "image": "test_lor"
    }
  ]
}
```

## Medications

* ### Get medications

This endpoint returns a list of all the medications.

```curl
  GET	http://localhost:3001/api/medications
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| uuid | It is the identification of medication | string | (Required) |
| name | It is the name of medication | string | (Required) |
| weight | the weight of medication | number | (Required) |
| code | the code of medication | string | (Required) |
| image | the image of medication | string | (Required) |

#### Example JSON Response
```json
[
  {
    "uuid": "b389c69c-8aa8-437c-9729-5e7706ce7cca",
    "name": "CLOTRIMAZOL",
    "weight": 100,
    "code": "TEST_CLO_123",
    "image": "test"
  },
  {
    "uuid": "cd37d839-a2ac-4f30-b676-577ea2f4f3de",
    "name": "Omeprazol",
    "weight": 150,
    "code": "TEST_OME_123",
    "image": "test_ome"
  },
  {
    "uuid": "a93c2c92-3706-429c-8fc1-bcb796586056",
    "name": "Loratadina",
    "weight": 50,
    "code": "TEST_LOR_123",
    "image": "test_lor"
  }
]
```

* ### Get a medication
This endpoint returns a medication by uuid.

```curl
  GET	http://localhost:3001/api/medication/:uuid
```

### Example Request
```curl
  GET	http://localhost:3001/api/medication/b389c69c-8aa8-437c-9729-5e7706ce7cca
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| uuid | It is the identification of medication | string | (Required) |
| name | It is the name of medication | string | (Required) |
| weight | the weight of medication | number | (Required) |
| code | the code of medication | string | (Required) |
| image | the image of medication | string | (Required) |

#### Example JSON Response
```json
{
  "uuid": "b389c69c-8aa8-437c-9729-5e7706ce7cca",
  "name": "CLOTRIMAZOL",
  "weight": 100,
  "code": "TEST_CLO_123",
  "image": "test"
}
```

* ### Create a medication

This endpoint creates a new medication.

```curl
  POST	http://localhost:3001/api/medications
```

#### Body request
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| name | It is the name of medication | string | (Required) |
| weight | the weight of medication | number | (Required) |
| code | the code of medication | string | (Required) |
| image | the image of medication | string | (Required) |

#### Example JSON Request
```json
{
  "name": "CLOTRIMAZOL",
  "weight": 100,
  "code": "TEST_CLO_123",
  "image": "test"
}
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| uuid | It is the identification of medication | string | (Required) |
| name | It is the name of medication | string | (Required) |
| weight | the weight of medication | number | (Required) |
| code | the code of medication | string | (Required) |
| image | the image of medication | string | (Required) |

#### Example JSON Response
```json
{
  "uuid": "b389c69c-8aa8-437c-9729-5e7706ce7cca",
  "name": "CLOTRIMAZOL",
  "weight": 100,
  "code": "TEST_CLO_123",
  "image": "test"
}
```

* ### Update a medication
This endpoint updates all medication information.

```curl
  PUT	http://localhost:3001/api/medication/:uuid
```

#### Example Request
```curl
  PUT	http://localhost:3001/api/medication/b389c69c-8aa8-437c-9729-5e7706ce7cca
```

#### Body request
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| name | It is the name of medication | string | (Required) |
| weight | the weight of medication | number | (Required) |
| code | the code of medication | string | (Required) |
| image | the image of medication | string | (Required) |

#### Example JSON Request
```json
{
  "name": "CLOTRIMAZOL",
  "weight": 100,
  "code": "TEST_CLO_123",
  "image": "test"
}
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| uuid | It is the identification of medication | string | (Required) |
| name | It is the name of medication | string | (Required) |
| weight | the weight of medication | number | (Required) |
| code | the code of medication | string | (Required) |
| image | the image of medication | string | (Required) |

#### Example JSON Response
```json
{
  "uuid": "b389c69c-8aa8-437c-9729-5e7706ce7cca",
  "name": "CLOTRIMAZOL",
  "weight": 100,
  "code": "TEST_CLO_123",
  "image": "test"
}
```

* ### Delete a medication

This endpoint deletes a medication by uuid.

```curl
  DELETE	http://localhost:3001/api/medication/:uuid
```

### Example Request
```curl
  DELETE	http://localhost:3001/api/medication/b389c69c-8aa8-437c-9729-5e7706ce7cca
```

#### Body response
| Key | Description | Type | Rules |
|-----|-------------|------|-------|
| message | It is the message of the response | string | (Required) |

#### Example JSON Response
```json
{
  "message": "Medication deleted successfully"
}
```