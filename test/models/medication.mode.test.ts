import { ValidationError } from '../../src/helpers/errors';
import { Medication } from '../../src/models/medication.model';
import { IMedication } from '../../src/types/medication.type';

jest.mock('../../src/helpers/errors', () => ({
  ValidationError: jest.fn(),
}));

describe('Create Medication class', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create a medication instance', () => {
    const medication = new Medication({
      name: 'test',
      weight: 1,
      code: 'TEST',
      image: 'test.png',
    } as IMedication);

    expect(medication.uuid).toBeDefined();
    expect(medication.name).toBe('test');
  });

  it('should throw ValidationError if required parameters are missing', () => {
    // @ts-ignore: Intentionally passing undefined parameters for testing
    new Medication({});
    expect(ValidationError).toHaveBeenCalledWith('The medication parameters are missing');
  });

  it('should throw ValidationError if code is invalid', () => {
    new Medication({
      name: 'test',
      weight: 1,
      code: 'test',
      image: 'test.png',
    } as IMedication);
    expect(ValidationError).toHaveBeenCalledWith('Invalid medication parameters');
  });
});