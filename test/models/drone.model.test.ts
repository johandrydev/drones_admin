import { Drone } from '../../src/models/drone.model';
import { ValidationError } from '../../src/helpers/errors';
import { DroneModel, DroneState, IDrone } from '../../src/types/drone.type';
import { Medication } from '../../src/models/medication.model';

jest.mock('../../src/helpers/errors', () => ({
  ValidationError: jest.fn(),
}));

describe('Create Drone class', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create a drone instance', () => {
    const drone = new Drone({
      model: DroneModel.Heavyweight,
      weightLimit: 500,
      batteryCapacity: 100,
      state: DroneState.IDLE,
    } as IDrone);

    expect(drone.serialNumber).toBeDefined();
    expect(drone.model).toBe(DroneModel.Heavyweight);
    expect(drone.items.length).toBe(0);
  });

  it('should throw ValidationError if required parameters are missing', () => {
    // @ts-ignore: Intentionally passing undefined parameters for testing
    new Drone({});
    expect(ValidationError).toHaveBeenCalledWith('The drone parameters are missing');
  });

  it('should throw ValidationError if batteryCapacity is more than 100', () => {
    new Drone({
      model: DroneModel.Heavyweight,
      weightLimit: 500,
      batteryCapacity: 101,
      state: DroneState.IDLE,
    } as IDrone);
    expect(ValidationError).toHaveBeenCalledWith('Invalid drone parameters');
  });
});

describe('Add medications to drone', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should add medications to drone', () => {
    const drone = new Drone({
      model: DroneModel.Heavyweight,
      weightLimit: 500,
      batteryCapacity: 100,
      state: DroneState.IDLE,
    } as IDrone);

    drone.addMedications([
      {
        name: 'test',
        weight: 100,
        code: 'TEST',
        image: 'test.png',
      },
    ] as Medication[]);

    expect(drone.items.length).toBe(1);
    expect(drone.items[0].name).toBe('test');
  });

  it('should throw ValidationError if batteryCapacity is less than 25', () => {
    const drone = new Drone({
      model: DroneModel.Heavyweight,
      weightLimit: 500,
      batteryCapacity: 24,
      state: DroneState.IDLE,
    } as IDrone);

    drone.addMedications([
      {
        name: 'test',
        weight: 100,
        code: 'TEST',
        image: 'test.png',
      },
    ] as Medication[]);

    expect(ValidationError).toHaveBeenCalledWith('The battery capacity of the drone is too low to deliver the medications');
  });

  it('should throw ValidationError if weight of the medications exceeds the weight limit of the drone', () => {
    const drone = new Drone({
      model: DroneModel.Lightweight,
      weightLimit: 200,
      batteryCapacity: 100,
      state: DroneState.IDLE,
    } as IDrone);

    drone.addMedications([
      {
        name: 'test',
        weight: 300,
        code: 'TEST',
        image: 'test.png',
      },
    ] as Medication[]);

    expect(ValidationError).toHaveBeenCalledWith('The weight of the medications exceeds the weight limit of the drone');
  });
});