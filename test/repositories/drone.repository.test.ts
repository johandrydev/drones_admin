import { DroneRepository } from '../../src/repositories/drone.repository';
import { RedisService } from '../../src/services/redis';
import { DroneModel, DroneState, IDrone } from '../../src/types/drone.type';

describe('DroneRepository', () => {
  let droneRepository: DroneRepository;
  let redisService: RedisService;

  // mock redisService
  class MockRedisService {
    get = jest.fn();
    set = jest.fn();
  }

  beforeEach(() => {
    redisService = new MockRedisService() as any;
    droneRepository = new DroneRepository(redisService);
    jest.clearAllMocks();
  });

  const mockDrones: IDrone[] = [
    {
      serialNumber: '1',
      model: DroneModel.Lightweight,
      weightLimit: 100,
      batteryCapacity: 100,
      state: DroneState.IDLE,
      items: [],
    },
    {
      serialNumber: '2',
      model: DroneModel.Lightweight,
      weightLimit: 100,
      batteryCapacity: 100,
      state: DroneState.IDLE,
      items: [],
    },
    {
      serialNumber: '3',
      model: DroneModel.Lightweight,
      weightLimit: 100,
      batteryCapacity: 100,
      state: DroneState.LOADING,
      items: [],
    },
  ];
  describe('getDrones', () => {
    it('should return all drones', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const result = await droneRepository.getDrones();

      expect(result).toEqual(mockDrones);
      expect(redisService.get).toBeCalledWith('drones');
    });

    it('should return all drones by state', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const result = await droneRepository.getDrones(DroneState.IDLE);

      expect(result.length).toEqual(2);
      expect(redisService.get).toBeCalledWith('drones');
    });
  });

  describe('getDroneBySerialNumber', () => {
    it('should return drone by serial number', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const result = await droneRepository.getDroneBySerialNumber('1');

      expect(result).toEqual(mockDrones[0]);
      expect(redisService.get).toBeCalledWith('drones');
    });

    it('should return null if drone does not exist', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const result = await droneRepository.getDroneBySerialNumber('4');

      expect(result).toBeNull();
      expect(redisService.get).toBeCalledWith('drones');
    });
  });

  describe('createDrone', () => {
    it('should create drone', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const newDrone: IDrone = {
        serialNumber: '4',
        model: DroneModel.Lightweight,
        weightLimit: 100,
        batteryCapacity: 100,
        state: DroneState.IDLE,
        items: []
      };

      const result = await droneRepository.createDrone(newDrone);

      expect(result.serialNumber).toEqual(newDrone.serialNumber);
      expect(redisService.get).toBeCalledWith('drones');
      expect(redisService.set).toBeCalledWith('drones', JSON.stringify([...mockDrones, newDrone]));
    });
  });

  describe('updateDrone', () => {
    it('should update drone', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const updatedDrone: IDrone = {
        serialNumber: '1',
        model: DroneModel.Lightweight,
        weightLimit: 100,
        batteryCapacity: 100,
        state: DroneState.LOADING,
        items: [],
      };

      const result = await droneRepository.updateDrone('1', updatedDrone);

      expect(result?.state).toEqual(updatedDrone.state);
      expect(redisService.get).toBeCalledWith('drones');
      expect(redisService.set).toBeCalledWith('drones', JSON.stringify([updatedDrone, mockDrones[1], mockDrones[2]]));
    });

    it('should return null if drone does not exist', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const updatedDrone: IDrone = {
        serialNumber: '4',
        model: DroneModel.Lightweight,
        weightLimit: 100,
        batteryCapacity: 100,
        state: DroneState.LOADING,
        items: [],
      };

      const result = await droneRepository.updateDrone('4', updatedDrone);

      expect(result).toBeNull();
      expect(redisService.get).toBeCalledWith('drones');
    });
  });

  describe('deleteDrone', () => {
    it('should delete drone', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const result = await droneRepository.deleteDrone('1');

      expect(result?.serialNumber).toEqual('1');
      expect(redisService.get).toBeCalledWith('drones');
      expect(redisService.set).toBeCalledWith('drones', JSON.stringify([mockDrones[1], mockDrones[2]]));
    });

    it('should return null if drone does not exist', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const result = await droneRepository.deleteDrone('4');

      expect(result).toBeNull();
      expect(redisService.get).toBeCalledWith('drones');
    });
  });

  describe('addMedicationsToDrone', () => {
    it('should add medications to drone', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const medications = [
        {
          uuid: '1',
          name: 'test',
          weight: 1,
          code: 'TEST',
          image: 'test.png',
        },
        {
          uuid: '2',
          name: 'test',
          weight: 1,
          code: 'TEST',
          image: 'test.png',
        },
      ];

      const result = await droneRepository.addMedicationsToDrone('1', medications);

      expect(result?.items?.length).toEqual(2);
      expect(redisService.get).toBeCalledWith('drones');
      expect(redisService.set).toBeCalledWith('drones', JSON.stringify([result, mockDrones[1], mockDrones[2]]));
    });

    it('should return null if drone does not exist', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockDrones));

      const medications = [
        {
          uuid: '1',
          name: 'test',
          weight: 1,
          code: 'TEST',
          image: 'test.png',
        },
        {
          uuid: '2',
          name: 'test',
          weight: 1,
          code: 'TEST',
          image: 'test.png',
        },
      ];

      const result = await droneRepository.addMedicationsToDrone('4', medications);

      expect(result).toBeNull();
      expect(redisService.get).toBeCalledWith('drones');
    });
  });
});