import { MedicationRepository } from '../../src/repositories/medication.repository';
import { IMedication } from '../../src/types/medication.type';

describe('MedicationRepository', () => {
  // mock redisService
  class MockRedisService {
    get = jest.fn();
    set = jest.fn();
  }

  let redisService: MockRedisService;
  let medicationRepository: MedicationRepository;

  beforeEach(() => {
    redisService = new MockRedisService();
    medicationRepository = new MedicationRepository(redisService as any);
    jest.clearAllMocks();
  });

  const mockMedications: IMedication[] = [
    {
      uuid: "b389c69c-8aa8-437c-9729-5e7706ce7cca",
      name: "CLOTRIMAZOL",
      weight: 100,
      code: "TEST_CLO_123",
      image: "CLOTRIMAZOL.png"
    },
    {
      uuid: "b389c69c-8aa8-437c-9729-5e7706ce7ccb",
      name: "Omeprazol",
      weight: 150,
      code: "TEST_OME_123",
      image: "Omeprazol.png"
    }
  ];

  describe('getMedications', () => {
    it('should return all medications', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockMedications));

      const result = await medicationRepository.getMedications();

      expect(result).toEqual(mockMedications);
      expect(redisService.get).toBeCalledWith('medications');
    });
  });

  describe('getMedicationByUuid', () => {
    it('should return medication by uuid', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockMedications));

      const result = await medicationRepository.getMedicationByUuid("b389c69c-8aa8-437c-9729-5e7706ce7cca");

      expect(result).toEqual(mockMedications[0]);
      expect(redisService.get).toBeCalledWith('medications');
    });

    it('should return null if medication not found', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockMedications));

      const result = await medicationRepository.getMedicationByUuid("b389c69c-8aa8-437c-9729-5e7706ce7ccc");

      expect(result).toBeNull();
      expect(redisService.get).toBeCalledWith('medications');
    });
  });

  describe('createMedication', () => {
    it('should create medication', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockMedications));

      const medication = {
        uuid: "b389c69c-8aa8-437c-9729-5e7706ce7ccc",
        name: "Loratadina",
        weight: 50,
        code: "TEST_LOR_123",
        image: "Loratadina.png"
      };

      const result = await medicationRepository.createMedication(medication);

      expect(result).toEqual(medication);
      expect(redisService.get).toBeCalledWith('medications');
      expect(redisService.set).toBeCalledWith('medications', JSON.stringify([...mockMedications, medication]));
    });
  });

  describe('updateMedication', () => {
    it('should update medication', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockMedications));

      const medication = {
        uuid: "b389c69c-8aa8-437c-9729-5e7706ce7ccb",
        name: "Omeprazol",
        weight: 150,
        code: "TEST_OME_1",
        image: "Omeprazol_test.png"
      };

      const result = await medicationRepository.updateMedication("b389c69c-8aa8-437c-9729-5e7706ce7ccb", medication);

      expect(result).toEqual(medication);
      expect(redisService.get).toBeCalledWith('medications');
      expect(redisService.set).toBeCalledWith('medications', JSON.stringify([mockMedications[0], medication]));
    });

    it('should return null if medication not found', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockMedications));

      const medication = {
        uuid: "b389c69c-8aa8-437c-9729-5e7706ce7ccc",
        name: "Loratadina",
        weight: 50,
        code: "TEST_LOR_123",
        image: "Loratadina.png"
      };

      const result = await medicationRepository.updateMedication("b389c69c-8aa8-437c-9729-5e7706ce7ccd", medication);

      expect(result).toBeNull();
      expect(redisService.get).toBeCalledWith('medications');
    });
  });

  describe('deleteMedication', () => {
    it('should delete medication', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockMedications));

      const result = await medicationRepository.deleteMedication("b389c69c-8aa8-437c-9729-5e7706ce7ccb");

      expect(result).toEqual(mockMedications[1]);
      expect(redisService.get).toBeCalledWith('medications');
      expect(redisService.set).toBeCalledWith('medications', JSON.stringify([mockMedications[0]]));
    });

    it('should return null if medication not found', async () => {
      (redisService.get as jest.Mock).mockResolvedValue(JSON.stringify(mockMedications));

      const result = await medicationRepository.deleteMedication("b389c69c-8aa8-437c-9729-5e7706ce7ccc");

      expect(result).toBeNull();
      expect(redisService.get).toBeCalledWith('medications');
    });
  });
});