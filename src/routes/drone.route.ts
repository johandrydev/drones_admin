// create reoutes for drone
import { Router } from "express";
import { DroneController } from "../controllers/drone.controller";
import { DroneRepository } from "../repositories/drone.repository";
import { RedisService } from "../services/redis";
import { checkBatteryLevels } from "../services/drone";
import { loadDataFromJson } from "../services/data";
import cron from "node-cron";

export const droneRoutes = Router();
const redisService = new RedisService();
loadDataFromJson('./data.json', redisService);

const droneRepository = new DroneRepository(redisService);
const droneController = new DroneController(droneRepository);

droneRoutes.route("/drones")
  .post(droneController.createDrone)
  .get(droneController.getDrones)

droneRoutes.route("/drones/:serialNumber")
  .get(droneController.getDroneBySerialNumber)
  .put(droneController.updateDrone)
  .delete(droneController.deleteDrone)
  .patch(droneController.addMedicationsToDrone)

// Schedule the battery check task to run every minute
const task = cron.schedule("* * * * *", () => checkBatteryLevels(droneRepository));
task.start();