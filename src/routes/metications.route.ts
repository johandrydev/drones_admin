// create reoutes for drone
import { Router } from "express";
import { RedisService } from "../services/redis";
import { MedicationRepository } from "../repositories/medication.repository";
import { MedicationController } from "../controllers/medication.controller";

export const medicationRoutes = Router();
const redisService = new RedisService();
const medicationRepository = new MedicationRepository(redisService);
const medication = new MedicationController(medicationRepository);

medicationRoutes.route("/medication")
  .post(medication.createMedication)
  .get(medication.getMedications)

medicationRoutes.route("/medication/:uuid")
  .get(medication.getMedicationByUuid)
  .put(medication.updateMedication)
  .delete(medication.deleteMedication)