import { NextFunction, Request, Response } from "express";
import { IDroneRepository } from "../types/drone.type";

export class DroneController {
  constructor(private droneRepository: IDroneRepository) { }

  createDrone = async ({ body }: Request, res: Response, next: NextFunction) => {
    try {
      const drone = await this.droneRepository.createDrone(body);
      res.status(201).send(drone);
    } catch (error) {
      next(error);
    }
  }

  updateDrone = async ({ body, params }: Request, res: Response, next: NextFunction) => {
    try {
      const drone = await this.droneRepository.updateDrone(params.serialNumber, body);
      if (!drone) {
        return res.status(404).send({ message: "Drone not found" });
      }
      res.send(drone);
    } catch (error) {
      next(error);
    }
  }

  getDrones = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const state = req.query?.state as string;
      const drones = await this.droneRepository.getDrones(state);
      res.send(drones);
    } catch (error) {
      next(error);
    }
  }

  getDroneBySerialNumber = async ({ params }: Request, res: Response, next: NextFunction) => {
    try {
      const drone = await this.droneRepository.getDroneBySerialNumber(params.serialNumber);
      if (!drone) {
        return res.status(404).send({ message: 'Drone not found' });
      }
      res.send(drone);
    } catch (error) {
      next(error);
    }
  }
  
  deleteDrone = async ({ params }: Request, res: Response, next: NextFunction) => {
    try {
      const drone = await this.droneRepository.deleteDrone(params.serialNumber);
      if (!drone) {
        return res.status(404).send({ message: 'Drone not found' });
      }
      res.send({ message: 'Drone deleted successfully'});
    } catch (error) {
      next(error);
    }
  }

  addMedicationsToDrone = async ({ body, params }: Request, res: Response, next: NextFunction) => {
    try {
      const drone = await this.droneRepository.addMedicationsToDrone(params.serialNumber, body);
      if (!drone) {
        return res.status(404).send({ message: 'Drone not found' });
      }
      res.send(drone);
    } catch (error) {
      next(error);
    }
  }
}