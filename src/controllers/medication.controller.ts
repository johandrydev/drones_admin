import { NextFunction, Request, Response } from "express";
import { IMedicationRepository } from "../types/medication.type";

export class MedicationController {
  constructor(private medicationRepository: IMedicationRepository) { }

  createMedication = async ({ body }: Request, res: Response, next: NextFunction) => {
    try {
      const medication = await this.medicationRepository.createMedication(body);
      res.status(201).send(medication);
    } catch (error) {
      next(error);
    }
  }

  updateMedication = async ({ body, params }: Request, res: Response, next: NextFunction) => {
    try {
      const medication = await this.medicationRepository.updateMedication(params.uuid, body);
      if (!medication) {
        return res.status(404).send({ message: "Medication not found" });
      }
      res.send(medication);
    } catch (error) {
      next(error);
    }
  }

  getMedications = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const medications = await this.medicationRepository.getMedications();
      res.send(medications);
    } catch (error) {
      next(error);
    }
  }

  getMedicationByUuid = async ({ params }: Request, res: Response, next: NextFunction) => {
    try {
      const medication = await this.medicationRepository.getMedicationByUuid(params.uuid);
      if (!medication) {
        return res.status(404).send({ message: 'Medication not found' });
      }
      res.send(medication);
    } catch (error) {
      next(error);
    }
  }

  deleteMedication = async ({ params }: Request, res: Response, next: NextFunction) => {
    try {
      const medication = await this.medicationRepository.deleteMedication(params.uuid);
      if (!medication) {
        return res.status(404).send({ message: 'Medication not found' });
      }
      res.send({ message: 'Medication deleted successfully'});
    } catch (error) {
      next(error);
    }
  }
}