import { Medication } from "../models/medication.model";

export enum DroneState {
  IDLE = 'IDLE',
  LOADING = 'LOADING',
  LOADED = 'LOADED',
  DELIVERING = 'DELIVERING',
  DELIVERED = 'DELIVERED',
  RETURNING = 'RETURNING'
};

export enum DroneModel {
  Lightweight = 'Lightweight',
  Middleweight = 'Middleweight',
  Cruiserweight = 'Cruiserweight',
  Heavyweight = 'Heavyweight'
};

export interface IDrone {
  serialNumber: string;
  model: DroneModel;
  weightLimit: number;
  batteryCapacity: number;
  state: DroneState;
  items?: Medication[];
};

export interface IDroneRepository {
  getDrones(state?: string): Promise<IDrone[]>;
  getDroneBySerialNumber(serialNumber: string): Promise<IDrone | null>;
  createDrone(drone: IDrone): Promise<IDrone>;
  updateDrone(serialNumber: string, drone: IDrone): Promise<IDrone | null>;
  deleteDrone(serialNumber: string): Promise<IDrone | null>;
  addMedicationsToDrone(serialNumber: string, medications: Medication[]): Promise<IDrone | null>;
}


