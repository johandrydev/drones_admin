export interface IMedication {
  uuid: string;
  name: string;
  weight: number;
  code: string;
  image: string;
};

export interface IMedicationRepository {
  createMedication(medication: IMedication): Promise<IMedication>;
  getMedicationByUuid(uuid: string): Promise<IMedication | null>;
  getMedications(): Promise<IMedication[]>;
  updateMedication(uuid: string, medication: IMedication): Promise<IMedication | null>;
  deleteMedication(uuid: string): Promise<IMedication | null>;
}
