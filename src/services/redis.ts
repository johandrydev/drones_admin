import { RedisClientType, createClient } from "redis";

export class RedisService {
  client: RedisClientType;

  constructor() {
    this.client = createClient({ url: 'redis://redis:6379' })
    // this.client = createClient()
    this.client.on("connect", () => {
      console.log("Connected to Redis");
    });
  }

  async set(key: string, value: string) {
    await this.client.connect();
    const result = await this.client.set(key, value);
    await this.client.disconnect();
    return result;
  }

  async get(key: string) {
    await this.client.connect();
    const value = await this.client.get(key);
    await this.client.disconnect();
    return value;
  }
}