import { RedisService } from './redis'
import fs from 'fs'

export const loadDataFromJson = async (path: string, redisService: RedisService) => {
  console.log('Loading data from json file')
  const data = fs.readFileSync(path, "utf8");
  const dataJson = JSON.parse(data);
  
  await redisService.set('drones', JSON.stringify(dataJson['drones']))
  await redisService.set('medications', JSON.stringify(dataJson['medications']))
  return data
}