import { DroneRepository } from '../repositories/drone.repository';
import fs from 'fs';

export const checkBatteryLevels = async (droneRepository: DroneRepository) => {
  console.log("Checking battery levels");
  const drones = await droneRepository.getDrones();
  drones.forEach((drone) => {
    const eventLog = {
      serialNumber: drone.serialNumber,
      batteryLevel: drone.batteryCapacity,
      timestamp: new Date(),
    };
    console.log("eventLog: ", eventLog);
    fs.appendFile('event-log.txt', JSON.stringify(eventLog) + "\n", (err) => {
      if (err) {
        console.error(err);
      }
    });
  });
};