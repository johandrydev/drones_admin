import { Medication } from "../models/medication.model";
import { RedisService } from "../services/redis";
import { IMedication, IMedicationRepository } from "../types/medication.type";

export class MedicationRepository implements IMedicationRepository {
  constructor(private redisService: RedisService) { }
  
  getMedications = async (): Promise<IMedication[]> => {
    const medicationsCache = await this.redisService.get("medications");
    return medicationsCache ? JSON.parse(medicationsCache) : [];
  }

  getMedicationByUuid = async (uuid: string): Promise<IMedication | null> => {
    const medications = await this.getMedications();
    const medication = medications.find((med) => med.uuid === uuid);
    return medication ?? null;
  }

  createMedication = async (medication: IMedication): Promise<IMedication> => {
    const medications = await this.getMedications();
    const newMedication = new Medication(medication);
    medications.push(newMedication);
    this.redisService.set("medications", JSON.stringify(medications));
    return newMedication;
  }

  updateMedication = async (uuid: string, medication: IMedication): Promise<IMedication | null> => {
    const medications = await this.getMedications();
    const medicationIndex = medications.findIndex((med) => med.uuid === uuid);
    if (medicationIndex === -1) {
      return null;
    }
    const updatedMedication = new Medication(medication);
    medications[medicationIndex] = updatedMedication;
    this.redisService.set("medications", JSON.stringify(medications));
    return updatedMedication;
  }

  deleteMedication = async (uuid: string): Promise<IMedication | null> => {
    const medications = await this.getMedications();
    const medicationIndex = medications.findIndex((med) => med.uuid === uuid);
    if (medicationIndex === -1) {
      return null;
    }
    const medication = medications[medicationIndex];
    medications.splice(medicationIndex, 1);
    this.redisService.set("medications", JSON.stringify(medications));
    return medication;
  }
}

