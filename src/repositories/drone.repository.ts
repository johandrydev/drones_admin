import { Drone } from "../models/drone.model";
import { IDrone, IDroneRepository } from "../types/drone.type";
import { RedisService } from "../services/redis";
import { Medication } from "../models/medication.model";

export class DroneRepository implements IDroneRepository {
  constructor(private redisService: RedisService) { }

  getDrones = async (state?: string): Promise<IDrone[]> => {
    const dronesCache = await this.redisService.get("drones");
    const drones = dronesCache ? JSON.parse(dronesCache) : [];

    if (state) {
      const dronesByState = drones.filter((drn: IDrone) => drn.state === state);
      return dronesByState;
    }

    return drones;
  }

  getDroneBySerialNumber = async (serialNumber: string): Promise<IDrone | null> => {
    const drones = await this.getDrones();
    const drone = drones.find((drn) => drn.serialNumber === serialNumber);
    return drone ?? null;
  }

  createDrone = async (drn: IDrone): Promise<IDrone> => {
    const drones = await this.getDrones();

    const drone = new Drone(drn);
    drones.push(drone);

    this.redisService.set("drones", JSON.stringify(drones));
    return drone;
  }

  updateDrone = async (serialNumber: string, drn: IDrone): Promise<IDrone | null> => {
    const drones = await this.getDrones();
    const droneIndex = drones.findIndex((drn) => drn.serialNumber === serialNumber);
    if (droneIndex === -1) {
      return null;
    }
    const updatedDrone = new Drone({ ...drn, serialNumber });
    drones[droneIndex] = updatedDrone;
    this.redisService.set("drones", JSON.stringify(drones));
    return updatedDrone;
  }

  deleteDrone = async (serialNumber: string): Promise<IDrone | null> => {
    const drones = await this.getDrones();
    const droneIndex = drones.findIndex((drn) => drn.serialNumber === serialNumber);
    if (droneIndex === -1) {
      return null;
    }
    const drone = drones[droneIndex];
    drones.splice(droneIndex, 1);
    this.redisService.set("drones", JSON.stringify(drones));
    return drone;
  }

  addMedicationsToDrone = async (serialNumber: string, medications: Medication[]): Promise<IDrone | null> => {
    const drones = await this.getDrones();
    const droneIndex = drones.findIndex((drn) => drn.serialNumber === serialNumber);
    if (droneIndex === -1) {
      return null;
    }
    const drone = new Drone(drones[droneIndex]);
    drone.addMedications(medications);
    drones[droneIndex] = drone;
    this.redisService.set("drones", JSON.stringify(drones));
    return drone;
  }
}