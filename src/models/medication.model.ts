import { v4 as uuidv4 } from "uuid";
import { IMedication } from "../types/medication.type";
import { ValidationError } from "../helpers/errors";

export class Medication implements IMedication {
  uuid: string;
  name: string;
  weight: number;
  code: string;
  image: string;

  constructor({ uuid, name, weight, code, image }: IMedication) {
    if (name === undefined || weight === undefined || code === undefined || image === undefined) {
      ValidationError("The medication parameters are missing");
      return;
    }

    // regex for code validation (allowed only upper case letters, underscore and numbers)
    const codeRegex = /^[A-Z0-9_]+$/;
    // regex for name validation (allowed only letters, numbers, ‘-‘, ‘_’)
    const nameRegex = /^[a-zA-Z0-9\-_]+$/;

    if (weight < 0 || !codeRegex.test(code) || !nameRegex.test(name)) {
      ValidationError("Invalid medication parameters");
      return;
    }

    this.uuid = uuid ?? uuidv4();
    this.name = name;
    this.weight = weight;
    this.code = code;
    this.image = image;
  };
}