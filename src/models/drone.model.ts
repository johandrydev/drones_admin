import { v4 as uuidv4 } from "uuid";
import { DroneModel, DroneState, IDrone } from "../types/drone.type";
import { ValidationError } from "../helpers/errors";
import { Medication } from "./medication.model";

export class Drone implements IDrone {
  serialNumber: string;
  model: DroneModel;
  weightLimit: number;
  batteryCapacity: number;
  state: DroneState;
  items: Medication[];

  constructor({ model, weightLimit, batteryCapacity, state, serialNumber, items }: IDrone) {
    if (model === undefined || weightLimit === undefined || batteryCapacity === undefined || state === undefined) {
      ValidationError("The drone parameters are missing");
      return;
    }

    if (batteryCapacity < 0 || batteryCapacity > 100 || weightLimit < 0 || weightLimit > 500 || !(model in DroneModel) || !(state in DroneState)) {
      ValidationError("Invalid drone parameters");
      return;
    }

    this.serialNumber = serialNumber ?? uuidv4();
    this.model = model;
    this.weightLimit = weightLimit;
    this.batteryCapacity = batteryCapacity;
    this.state = state;
    this.items = items ?? [];
  };

  addMedications = (medications: Medication[]): void => {
    // validation of the battery capacity if the drone can deliver the medications
    if (this.batteryCapacity < 25) {
      ValidationError("The battery capacity of the drone is too low to deliver the medications");
      return;
    }

    // validation of weight of the medications if they can be added to the drone
    const totalWeight = this.items.reduce((acc, item) => acc + item.weight, 0);
    const newWeight = medications.reduce((acc, item) => acc + item.weight, 0);
    if (totalWeight + newWeight > this.weightLimit) {
      ValidationError("The weight of the medications exceeds the weight limit of the drone");
      return;
    }

    this.state = (totalWeight + newWeight === this.weightLimit) ? DroneState.LOADED : DroneState.LOADING;
    this.items = [...medications, ...this.items];
  }

  removeMedications = (): void => {
    this.items = [];
  }
};